## Install libraries.
	`pip install -r requirements.txt`
	or
	`pip3 install -r requirements.txt`

## Run script
	`python script.py`
	or
	`python3 script.py`

## Result
	Find result_{mm_dd_yyyy}.xlsx file after script is finished.
	Result file will contains 3 sheets - member, office, member_office.