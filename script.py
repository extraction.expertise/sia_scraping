import os, re, time, random, json, requests, openpyxl
from get_email_tell import get_email_tell_text
from scrapy.selector import Selector

base_dir = os.path.dirname(os.path.abspath(__file__))
zip_lang_xlsx = f"{base_dir}/zip_language.xlsx"
result_xlsx = f"{base_dir}/result_{time.strftime('%m_%d_%Y')}.xlsx"

user_agent_list = [
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.83 Safari/537.1",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
    "Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
    "Mozilla/5.0 (en-us) AppleWebKit/534.14 (KHTML, like Gecko; Google Wireless Transcoder) Chrome/9.0.597 Safari/534.14 wimb_monitor.py/1.0",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36",
    "Mozilla/5.0 (Linux; Android 6.0.1; RedMi Note 5 Build/RB3N5C; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
]

class MemberOfficeScraper():
    # init
    def __init__(self):
        # root url
        self.root_url = "https://www.sia.ch/"

        # zip_language.xlsx data
        self.zip_lang_list = dict()

        # whole index list
        self.whole_index_list = {
            "member": list(),
            "office": list()
        }

        # result fields
        self.result_fields = {
            "member": ["ID", "URL", "LANGUAGE", "FULL_ADDRESS", "GENDER", "NAME", "EDUCATION", "ADDRESS", "CITY", "ZIP_CODE", "EMAIL", "TEL", "FAX", "WEBSITE", "JOB", "SECTOR", "GROUP", "SECTION"],
            "office": ["ID", "URL", "LANGUAGE", "FULL_ADDRESS", "NAME", "ADDRESS", "CITY", "ZIP_CODE", "EMAIL", "TEL", "FAX", "WEBSITE", "SECTOR"],
            "member_office": ["ID", "MEMBER_ID", "OFFICE_ID", "COLLECTED_AT"]
        }

        # check result file and create
        result_xlsx_exist = os.path.exists(result_xlsx)
        if not result_xlsx_exist:
            wb_obj = openpyxl.Workbook()
            sheet_obj =  wb_obj.active
            sheet_obj.title = "member"
            
            for index, fieldname in enumerate(self.result_fields["member"]):
                sheet_obj.cell(row=1, column=index+1).value = fieldname

            wb_obj.save(result_xlsx)

    # start
    def start(self):
        # read zip_lang.xlsx
        if os.path.exists(f"{base_dir}/zip_language.xlsx"):
            wb_obj = openpyxl.load_workbook(zip_lang_xlsx)
            sheet_obj = wb_obj.active
            max_row = sheet_obj.max_row

            for index in range(max_row):
                index = index + 1
                if index == 1:
                    continue
                
                zip = str(int(sheet_obj.cell(row=index, column=2).value)).strip()
                if zip not in self.zip_lang_list:
                    self.zip_lang_list[zip] = sheet_obj.cell(row=index, column=3).value.strip()

        # get members
        self.get_members()

        # get offices
        self.get_offices()

    # make request header
    def set_header(self):
        header = {
            'user-agent': random.choice(user_agent_list)
        }
        return header

    # format data 
    def format_dict(self, data, fields):
        for field in fields:
            if field not in data:
                data[field] = ""
        return data

    # get language by zip
    def get_lang_type(self, member_zip):
        if member_zip and member_zip in self.zip_lang_list:
            return self.zip_lang_list[member_zip]
        
        return "FR"

    def get_members(self):
        request_fr_link = "https://www.sia.ch/fr/affiliation/liste-des-membres/membres-individuels/"
        request_de_link = "https://www.sia.ch/de/mitgliedschaft/verzeichnis/einzelmitglieder/"
        request_it_link = "https://www.sia.ch/it/affiliazione/elenco-dei-membri/soci-individuali/"
        request_link = request_fr_link

        while True:
            members_link_list = list()
            member_dom = Selector(text = requests.get(request_link, headers=self.set_header(), timeout=10).text)
            for member in member_dom.xpath("//table[contains(@class, 'table-list-directory')]//tr"):
                try:
                    member_link = member.xpath("./td[1]/a[@href]/@href").get().strip()
                    member_zip = member.xpath("./td[4]/text()").get()
                    if member_zip:
                        member_zip = member_zip.strip()

                    item = dict()
                    item["zip"] = member_zip
                    member_link_subfix = f"m/{member_link.split('/m/')[1]}"
                    lang_type = self.get_lang_type(member_zip)

                    if lang_type == "FR":
                        member_link = request_fr_link + member_link_subfix
                    elif lang_type == "DE":
                        member_link = request_de_link + member_link_subfix
                    elif lang_type == "IT":
                        member_link = request_it_link + member_link_subfix
                    else:
                        continue

                    item["link"] = member_link
                    item["lang_type"] = lang_type
                    members_link_list.append(item)

                except:
                    continue
            
            
            # get member data
            self.get_members_data(members_link_list)

            next_page_link = member_dom.xpath("//span[@class='nextLinkWrap']/a/@href").get()
            if next_page_link:
                request_link = self.root_url + next_page_link
            else:
                break

    # get members data
    def get_members_data(self, members_link_list):
        for member_item in members_link_list:
            try:
                member_dom = Selector(text = requests.get(member_item["link"], headers=self.set_header(), timeout=10).text)
                member_dict = dict()
                member_dict["URL"] = member_item["link"]
                member_dict["LANGUAGE"] = member_item["lang_type"]
                member_dict["ZIP_CODE"] = member_item["zip"]

                contact_text = member_dom.xpath("//div[@class='contact-data']//@data-contact").get()
                sec_text = member_dom.xpath("//div[@class='secr']//@data-secr").get()
                email_tell_text = str(get_email_tell_text(contact_text, sec_text))
                email_tell_text = re.sub(r"\s+", " ", email_tell_text)
                email_tell_text = re.sub(r"\\n", "", email_tell_text)
                
                for item_t in email_tell_text.split("<br />"):
                    if "TEL" not in member_dict and re.search(r"\+\d+", item_t):
                        member_dict["TEL"] = item_t.strip()
                    elif "TEL" in member_dict and re.search(r"\+\d+", item_t):
                        member_dict["FAX"] = item_t.strip()
                    elif "WEBSITE" not in member_dict and any([re.search(r"www\.", item_t), re.search(r"http", item_t)]) and re.search(r"\>.*\<", item_t):
                        website = re.search(r"\>.*\<", item_t).group()
                        website = website[1:len(website)-1]
                        member_dict["WEBSITE"] = website
                    elif "EMAIL" not in member_dict and re.search(r"\@", item_t) and re.search(r"\>.*\<", item_t):
                        email = re.search(r"\>.*\<", item_t).group()
                        email = email[1:len(email)-1]
                        member_dict["EMAIL"] = email
            except:
                pass

            tr_count = 0
            member_attrs = member_dom.xpath("//div[@class='csc-default']//table//tr")
            member_attrs_len = len(member_attrs)
            for tr_ele in member_attrs:
                try:
                    if tr_ele.xpath("./th"):
                        tr_count += 1
                        continue

                    if tr_count == 1:
                        full_address_text = "\n".join(tr_ele.xpath("./td//text()").extract()).strip()
                        try:
                            member_dict["FULL_ADDRESS"] = re.sub(r"\s+", " ", full_address_text)
                        except:
                            pass

                        separated_full_address = full_address_text.split("\n")
                        zip_index = 0
                        gender_index = 0
                        if member_item["zip"]:
                            for index, full_address_item in enumerate(separated_full_address):
                                if member_item["zip"] in full_address_item:
                                    zip_index = index
                                    break

                        try:
                            gender = separated_full_address[gender_index].strip()
                            if gender in ['Frau', 'Madame', 'Herr', 'Monsieur', 'Signor', 'Signora']:
                                member_dict["GENDER"] = gender
                        except:
                            pass
                        
                        try:
                            unknown_text = separated_full_address[gender_index+1].strip()
                            if unknown_text in ['Frau', 'Madame', 'Herr', 'Monsieur', 'Signor', 'Signora']:
                                gender_index += 1
                                if member_dict["GENDER"]:
                                    pass
                                else:
                                    member_dict["GENDER"] = unknown_text
                        except:
                            pass

                        try:
                            if zip_index:
                                member_dict["NAME"] = separated_full_address[zip_index-3].strip()
                            else:
                                member_dict["NAME"] = separated_full_address[gender_index+1].strip()
                        except:
                            member_dict["NAME"] = ""

                        if not member_dict["NAME"]:
                            try:
                                member_dict["NAME"] = separated_full_address[gender_index+1].strip()
                            except:
                                pass

                        try:
                            if zip_index:
                                member_dict["EDUCATION"] = separated_full_address[zip_index-2].strip()
                            else:
                                member_dict["EDUCATION"] = separated_full_address[gender_index+2].strip()
                        except:
                            member_dict["EDUCATION"] = ""

                        if not member_dict["EDUCATION"]:
                            try:
                                member_dict["EDUCATION"] = separated_full_address[gender_index+2].strip()
                            except:
                                pass

                        try:
                            if zip_index:
                                member_dict["ADDRESS"] = separated_full_address[zip_index-1].strip()
                            else:
                                member_dict["ADDRESS"] = separated_full_address[gender_index+3].strip()
                        except:
                            member_dict["ADDRESS"] = ""

                        if not member_dict["ADDRESS"]:
                            try:
                                member_dict["ADDRESS"] = separated_full_address[gender_index+3].strip()
                            except:
                                pass

                        try:
                            if zip_index:
                                member_dict["CITY"] = separated_full_address[zip_index].strip().split(" ")[1]
                            else:
                                member_dict["CITY"] = separated_full_address[gender_index+4].strip().split(" ")[1]
                        except:
                            member_dict["CITY"] = "'"

                        if not member_dict["CITY"]:
                            try:
                                member_dict["CITY"] = separated_full_address[gender_index+4].strip().split(" ")[1]
                            except:
                                pass

                        
                    elif tr_count == 3:
                        detail_prefix = tr_ele.xpath("./td[1]/text()").get().strip()
                        if any([detail_prefix == item for item in ["Profession", "Beruf", "Professione"]]):
                            member_dict["JOB"] = tr_ele.xpath("./td[2]/text()").get().strip()
                        elif any([detail_prefix == item for item in ["Tätigkeitsbereich", "Domaine d’activité", "Settore d’attività"]]):
                            member_dict["SECTOR"] = tr_ele.xpath("./td[2]/text()").get().strip()
                        elif any([detail_prefix == item for item in ["Berufsgruppe", "Field of activity", "Settore d’attività", "Groupe professionnel"]]):
                            member_dict["GROUP"] = tr_ele.xpath("./td[2]/text()").get().strip()
                        elif any([detail_prefix == item for item in ["Sektion", "Section", "Sezione"]]):
                            member_dict["SECTION"] = tr_ele.xpath("./td[2]/text()").get().strip()
                        
                except:
                    continue
            
            formatted_dict = self.format_dict(member_dict, self.result_fields["member"])
            self.make_result(formatted_dict, "member")

    def get_offices(self):
        request_fr_link = "https://www.sia.ch/fr/affiliation/liste-des-membres/membres-bureaux/"
        request_de_link = "https://www.sia.ch/de/mitgliedschaft/verzeichnis/firmenmitglieder/"
        request_it_link = "https://www.sia.ch/it/affiliazione/elenco-dei-membri/ditte-sia/"
        request_link = request_fr_link

        while True:
            offices_link_list = list()
            office_dom = Selector(text = requests.get(request_link, headers=self.set_header(), timeout=10).text)
            for office in office_dom.xpath("//table[contains(@class, 'table-list-directory')]//tr"):
                try:
                    office_link = office.xpath("./td[1]/a[@href]/@href").get().strip()
                    office_zip = office.xpath("./td[4]/text()").get()
                    if office_zip:
                        office_zip = office_zip.strip()

                    item = dict()
                    item["zip"] = office_zip
                    office_link_subfix = f"m/{office_link.split('/m/')[1]}"
                    lang_type = self.get_lang_type(office_zip)

                    if lang_type == "FR":
                        office_link = request_fr_link + office_link_subfix
                    elif lang_type == "DE":
                        office_link = request_de_link + office_link_subfix
                    elif lang_type == "IT":
                        office_link = request_it_link + office_link_subfix
                    else:
                        continue

                    item["link"] = office_link
                    item["lang_type"] = lang_type
                    offices_link_list.append(item)

                except:
                    continue
            
            
            # get office data
            self.get_offices_data(offices_link_list)

            next_page_link = office_dom.xpath("//span[@class='nextLinkWrap']/a/@href").get()
            if next_page_link:
                request_link = self.root_url + next_page_link
            else:
                break

    # get offices data
    def get_offices_data(self, offices_link_list):
        for office_item in offices_link_list:
            try:
                office_dom = Selector(text = requests.get(office_item["link"], headers=self.set_header(), timeout=10).text)
                office_dict = dict()
                office_dict["URL"] = office_item["link"]
                office_dict["LANGUAGE"] = office_item["lang_type"]
                office_dict["ZIP_CODE"] = office_item["zip"]
                office_dict["MEMBERS"] = list()

                contact_text = office_dom.xpath("//div[@class='contact-data']//@data-contact").get()
                sec_text = office_dom.xpath("//div[@class='secr']//@data-secr").get()
                email_tell_text = str(get_email_tell_text(contact_text, sec_text))
                email_tell_text = re.sub(r"\s+", " ", email_tell_text)
                email_tell_text = re.sub(r"\\n", "", email_tell_text)

                for item_t in email_tell_text.split("<br />"):
                    if "TEL" not in office_dict and re.search(r"\+\d+", item_t):
                        office_dict["TEL"] = item_t.strip()
                    elif "TEL" in office_dict and re.search(r"\+\d+", item_t):
                        office_dict["FAX"] = item_t.strip()
                    elif "WEBSITE" not in office_dict and any([re.search(r"www\.", item_t), re.search(r"http", item_t)]) and re.search(r"\>.*\<", item_t):
                        website = re.search(r"\>.*\<", item_t).group()
                        website = website[1:len(website)-1]
                        office_dict["WEBSITE"] = website
                    elif "EMAIL" not in office_dict and re.search(r"\@", item_t) and re.search(r"\>.*\<", item_t):
                        email = re.search(r"\>.*\<", item_t).group()
                        email = email[1:len(email)-1]
                        office_dict["EMAIL"] = email

            except:
                pass

            tr_count = 0
            member_tr_flag = False
            for tr_ele in office_dom.xpath("//div[@class='csc-default']//table//tr"):
                try:
                    if tr_ele.xpath("./th"):
                        tr_count += 1

                        if any(th_text_item in tr_ele.xpath("./th//text()").get() for th_text_item in ["Membre individuel", "Leitende", "Membro individuale"]):
                            member_tr_flag = True
                        else:
                            member_tr_flag = False
                        continue

                    if tr_count == 1:
                        full_address_text = "\n".join(tr_ele.xpath("./td//text()").extract()).strip()
                        try:
                            office_dict["FULL_ADDRESS"] = re.sub(r"\s+", " ", full_address_text)
                        except:
                            pass

                        separated_full_address = full_address_text.split("\n")
                        zip_index = len(separated_full_address) - 1
                        if office_item["zip"]:
                            for index, full_address_item in enumerate(separated_full_address):
                                if office_item["zip"] in full_address_item:
                                    zip_index = index
                                    break

                        office_dict["NAME"] = separated_full_address[0].strip()

                        try:
                            office_dict["ADDRESS"] = separated_full_address[zip_index-1].strip()
                        except:
                            office_dict["ADDRESS"] = ""

                        if not office_dict["ADDRESS"]:
                            try:
                                office_dict["ADDRESS"] = separated_full_address[1].strip()
                            except:
                                pass

                        try:
                            office_dict["CITY"] = separated_full_address[zip_index].strip().split(" ")[1]
                        except:
                            office_dict["CITY"] = "'"

                        if not office_dict["CITY"]:
                            try:
                                office_dict["CITY"] = separated_full_address[2].strip().split(" ")[1]
                            except:
                                pass

                    elif tr_count == 3:
                        sector_text = ", ".join(tr_ele.xpath("./td/ul/li//text()").extract())
                        office_dict["SECTOR"] = sector_text

                    if member_tr_flag:
                        for member_link in tr_ele.xpath("./td/a[@href]/@href").extract():
                            member_id = self.get_id(member_link)
                            office_dict["MEMBERS"].append(member_id)
                            
                except:
                    continue

            formatted_dict = self.format_dict(office_dict, self.result_fields["office"])

            self.make_result(formatted_dict, "office")

    # get unique id from url
    def get_id(self, link):
        return link.split("?")[0].split("/m/")[1].split('/')[0]
        
    # store data to result xlsx
    def make_result(self, data, _type):
        wb_obj = openpyxl.load_workbook(result_xlsx)
        sheet_create_flag = False
        if _type not in wb_obj.sheetnames:
            wb_obj.create_sheet(_type)
            sheet_create_flag = True
        sheet_obj = wb_obj[_type]

        if sheet_create_flag:
            for index, fieldname in enumerate(self.result_fields[_type]):
                sheet_obj.cell(row=1, column=index+1).value = fieldname

        # get max column count
        max_row = sheet_obj.max_row
        data["ID"] = max_row

        if _type in self.whole_index_list:
            self.whole_index_list[_type].append({"ID":data["ID"], "TYPE_ID":self.get_id(data["URL"])})

        print(f"----------------{_type} item-------------------")
        print(json.dumps(data, indent=2))

        for i in range(len(self.result_fields[_type])):
            sheet_obj.cell(row=max_row+1, column=i+1).value = data[self.result_fields[_type][i]]

        wb_obj.save(result_xlsx)

        if _type == "office":
            for member_index in self.whole_index_list["member"]:
                if member_index["TYPE_ID"] in data["MEMBERS"]:
                    self.make_result({"MEMBER_ID":member_index["ID"], "OFFICE_ID":data["ID"], "COLLECTED_AT": time.strftime("%m/%d/%Y %I:%M:%S %p")}, "member_office")
                    break

if __name__ == '__main__':
    # existing file delete
    if os.path.exists(result_xlsx):
        os.remove(result_xlsx)

    scraper = MemberOfficeScraper()
    scraper.start()
